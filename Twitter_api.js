var mysql = require("mysql");
var async = require('async');
var express = require('express');
var morgan = require('morgan');
var bodyParser = require('body-parser')

var app = express();
app.use(morgan('dev'));
app.use(bodyParser.json())
var con = mysql.createConnection({
  host: "localhost",
  user: "guest",
  password: "guest123",
  database:"mysql"
});

con.connect(function(err){
  if(err){
    console.log('Error connecting to Db');
    return;
  }
  console.log('Connection established');
});

var Twitter = require('twitter-js-client').Twitter;
var config = {
        "consumerKey": "yYuc6vOztOc9szJm3325Y8ZRY",
        "consumerSecret": "cAPqpPiDUFKbCF36yRJzWUIgRVzZtO2kZxxGqekZmzCnBAdSxD",
        "accessToken": "3964549877-8BbFKBOnjcHJpBMu9zJWKfBMrpdRGDWyMDkT6uD",
        "accessTokenSecret": "A8ILJYA7jnQoyBqzpQSaoQ0r6CRt35A8ePpFVWy7iiOgm"
    }

var twitter = new Twitter(config);

function data_grabber(twitter_obj,callback){

  twitter.getUserTimeline(twitter_obj,function (err, response, body) {
          console.log(err);
          callback(null);
  }, function(data){

    callback(null,data);

  });
}

function data_packager(data,callback){

  var obj=JSON.parse(data);
  var items=[];
  for(var i=0;i<obj.length;i++){

        var teet = {
            Tweet_id: obj[i].id,
            username: obj[i].user.screen_name,
            text:obj[i].text,
            Timestamp: obj[i].created_at
        };

            items.push(teet);
}
      callback(null,items);
}

function inserter (data,callback){
var count=0;
async.whilst(
  function(){ return count<data.length; },

  function(cback){
    var sample=data[count];

    con.query('INSERT INTO tweet SET ?', [sample], function(err) {
      console.log('Insertion done -->',count+1);
      count++;
      cback(null,count);
    });
  },

  function(err){
    callback(null);
  }

);
}

function query_from_url(data,callback){
  var username=data.screen_name;

  con.query('select * from tweet where username= ?', username, function(err,results) {
    if(err) console.log(err);

   var op={ "username":username,'Number of Tweets':results.length,'Tweets':results};
    callback(null,op);
  });
}

app.post('/process_tweets', function (req, res) {

   var twitter_obj = {
       screen_name:req.body.screen_name,
       count:req.body.count
   };

   async.waterfall([

   function(callback){
     data_grabber(twitter_obj,callback);
   },

   data_packager,
   inserter

   ],
   function (err){
     if(err) console.log(err);
     res.send("Tweets inserted in database successfully\n");

   });


});

app.post('/query_tweets', function (req, res) {

   var twitter_query_obj = {
       screen_name:req.body.screen_name
   };
     query_from_url(twitter_query_obj,function(err,results){
       if(err)console.log(err);
       res.send(results)
     });
});


var server = app.listen(8081, function () {

  var host = server.address().address
  var port = server.address().port

  console.log("Baba's Twitter api listening at http://%s:%s", host, port)

})
